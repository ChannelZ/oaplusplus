/*
================================================================================
Copyright (C) 2010 Jessica Allan
Quake III Arena, OpenArena or OA++ source code is free software; you can
redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation; either version 2 of the
License, or (at your option) any later version.

This source code is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
the source code; if not, write to the Free Software Foundation, Inc., 51
Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
================================================================================
*/

void PM_UpdateSettings( void ) {
	if (gameplay.string == "vq3") { // vq3 settings
		pm_stopspeed = 100.0f;
		pm_duckScale = 0.25f;
		pm_swimScale = 0.50f;
		pm_wadeScale = 0.70f;
		pm_accelerate = 10.0f;
		pm_airaccelerate = 1.0f;
		pm_wateraccelerate = 4.0f;
		pm_flyaccelerate = 8.0f;
		pm_friction = 6.0f;
		pm_waterfriction = 1.0f;
		pm_flightfriction = 3.0f;
		pm_spectatorfriction = 5.0f;
		cpm_pm_airstopaccelerate = 1;
		cpm_pm_aircontrol = 0;
		cpm_pm_strafeaccelerate = 1;
		cpm_pm_wishspeed = 400;
	} else if (gameplay.string == "cpm") { // cpm settings
		pm_stopspeed = 100.0f;
		pm_duckScale = 0.25f;
		pm_swimScale = 0.66f;
		pm_wadeScale = 0.78f;
		pm_accelerate = 15.0f;
		pm_airaccelerate = 1.0f;
		pm_wateraccelerate = 4.0f;
		pm_flyaccelerate = 8.0f;
		pm_friction = 8.0f;
		pm_waterfriction = 1.0f;
		pm_flightfriction = 3.0f;
		pm_spectatorfriction = 5.0f;
		cpm_pm_airstopaccelerate = 2.5;
		cpm_pm_aircontrol = 150;
		cpm_pm_strafeaccelerate = 70;
		cpm_pm_wishspeed = 30;
	} else if (gameplay.string == "epk") { // epk settings
		pm_stopspeed = 100.0f;
		pm_duckScale = 0.25f;
		pm_swimScale = 0.60f;
		pm_wadeScale = 0.80f;
		pm_accelerate = 30.0f;
		pm_airaccelerate = 1.0f;
		pm_wateraccelerate = 3.0f;
		pm_flyaccelerate = 8.0f;
		pm_friction = 16.0f;
		pm_waterfriction = 2.0f;
		pm_flightfriction = 3.0f;
		pm_spectatorfriction = 5.0f;
		cpm_pm_airstopaccelerate = 1;
		cpm_pm_aircontrol = 0;
		cpm_pm_strafeaccelerate = 1;
		cpm_pm_wishspeed = 400;
	}
}
